function getParameterByName( name ){
    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var regexS = "[\\?&]"+name+"=([^&#]*)";
    var regex = new RegExp( regexS );
    var results = regex.exec( window.location.href );
    if( results == null )
      return "";
    else
      return decodeURIComponent(results[1].replace(/\+/g, " "));
  }


document.addEventListener('DOMContentLoaded', function () {
    $('#summernote').summernote({
        placeholder: 'Write something...',
        toolbar: false,
        disableDragAndDrop: true
    });
    $('#summernote').summernote('disable');
    document.getElementsByClassName('note-editable')[0].setAttribute('style', 'background-color: white; height: 800px;');
    //let hiddenLoginRole = document.getElementById('hiddenLoginRole').value;
    //let hiddenLoginUser = document.getElementById('hiddenLoginUser').value;
    //console.log(hiddenLoginRole,hiddenLoginUser );
    

    let _id = getParameterByName('id');



    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/database",
        "method": "POST",
        "headers": {
            "Content-Type": "application/x-www-form-urlencoded",
            "Cache-Control": "no-cache",
            "Postman-Token": "c19cbb8e-e574-4724-9d90-64c6aba95f5b"
        },
        "data": {
            "func": "select",
            "table": "job",
            "_id":_id
        }
    }

    $.ajax(settings).done(function (response) {
        $('#summernote').summernote('code',response[0].job);
        document.getElementById('title').innerText=response[0].title;
        var share =document.getElementsByClassName('sharethis-inline-share-buttons')[0]; 
        if (!!share)
        {
            share.setAttribute('data-title',response[0].title);
        }
    });
   


}, false);