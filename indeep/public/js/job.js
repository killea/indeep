function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}
var app = angular.module('ui.job', ['ngAnimate', 'ngSanitize', 'ui.bootstrap']);
app.controller('PaginationDemoCtrl', function ($scope, $http, $log, $filter) {

    let where = getParameterByName('where');
    let what = getParameterByName('what');

    if (where == null) where = '';
    if (what == null) what = '';
    $http({
        method: 'POST',
        url: '/database',
        "data": {
            "func": "select",
            "table": "job",
            "what": what,
            "where": where
        }
    }).then(function successCallback(response) {
        //console.log(response.data);
        $scope.data = response.data;
        $scope.itemsPerPage = 10;
        $scope.totalItems = $scope.data.length;
        $scope.currentPage = 1;
        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };
        $scope.pageChanged = function () {
            $log.log('Page changed to: ' + $scope.currentPage);
        };
        $scope.maxSize = 5;

    }, function errorCallback(response) {
        console.log(response);
        swal(
            'Get files list failed! Code:031;Page:Job',
            'Server internal error',
            'warning'
        )
    });

    $scope.apply = function (event) {
        buttonStr = document.getElementById('apply').innerText.trim();

        if (buttonStr == 'Apply this job') {
            let hiddenLoginRole = document.getElementById('hiddenLoginRole').value;

            
            if (hiddenLoginRole == 'user') {
                console.log('can apply');
                console.log($('select[name=inputGroupSelect01]').val());

                if ($('select[name=inputGroupSelect01]').val() == '') {
                    swal(
                        'You have to select at least one file!',
                        'Need select a file!',
                        'warning'
                    )
                    return;
                }
                let lastSelected = document.getElementsByClassName('selected')[0];

                console.log(lastSelected.firstChild.innerText, lastSelected.firstChild.nextSibling.innerText);
                $http({
                    method: 'POST',
                    url: '/database',
                    "data": {
                        "func": "insert",
                        "table": "sendcv",
                        "file1": $('select[name=inputGroupSelect01]').val(),
                        "file2": $('select[name=inputGroupSelect02]').val(),
                        "user": document.getElementById('hiddenLoginUser').value,
                        "boss": lastSelected.firstChild.nextSibling.innerText,
                        "jobid": lastSelected.firstChild.innerText
                    }
                }).then(function successCallback(response) {
                    swal(
                        'You have suecessfully applyed this job!',
                        'Congrats!',
                        'info'
                    )
                    document.getElementById('apply').innerText = 'Cancel your application';
                    var select1 = document.getElementById("inputGroupSelect01");
                    var select2 = document.getElementById("inputGroupSelect02");
                    select1.disabled = true;
                    select2.disabled = true;
                }, function errorCallback(response) {
                    swal(
                        'Operation failed! Code:081;Page:Job::Apply',
                        'Server internal error',
                        'warning'
                    )


                });


            } else {
                console.log('can not apply this job');


            }
        } 
        else { // canel the applicaion
            let lastSelected = document.getElementsByClassName('selected')[0];
            $http({
                method: 'POST',
                url: '/database',
                "data": {
                    "func": "delete",
                    "table": "sendcv",
                    "file1": $('select[name=inputGroupSelect01]').val(),
                    "file2": $('select[name=inputGroupSelect02]').val(),
                    "user": document.getElementById('hiddenLoginUser').value,
                    "boss": lastSelected.firstChild.nextSibling.innerText,
                    "jobid": lastSelected.firstChild.innerText
                }
            }).then(function successCallback(response) {
                swal(
                    'You applicaion has been cancelled!',
                    'Cancelled',
                    'info'
                )
                document.getElementById('apply').innerText = 'Apply this job';
                var select1 = document.getElementById("inputGroupSelect01");
                var select2 = document.getElementById("inputGroupSelect02");
                select1.disabled = false;
                select2.disabled = false;
            }, function errorCallback(response) {
                swal(
                    'Operation failed! Code:081;Page:Job::CANCEL',
                    'Server internal error',
                    'warning'
                )
            })
        }

    }



    $scope.linesDone = function (event) {

        let firstLine = angular.element(document.getElementsByClassName('line')[0]);
        if (firstLine) {
            firstLine.removeClass('line');
            firstLine.addClass('selected');
        }
        //console.log(firstLine)
        //this is a hack, wait angular to finish rendering page
        setTimeout(function () {
            let firstLine = document.getElementsByClassName('selected')[0];
            var obj = $filter('filter')($scope.data, {
                _id: firstLine.firstChild.innerText
            }, true)[0];

            //console.log(obj);
            let hiddenLoginUser = document.getElementById('hiddenLoginUser').value;
            

            var share = document.getElementsByClassName('sharethis-inline-share-buttons')[0];
            if (!!share) {
                share.setAttribute('data-url', 'http://indeep.tk/jobdetail?id=' + obj._id);
                share.setAttribute('data-title', obj.title);
            }



            console.log('hiddenLoginRole=',hiddenLoginRole);

            $http({
                method: 'POST',
                url: '/database',
                "data": {
                    "func": "select",
                    "table": "job",
                    "_id": obj._id
                }
            }).then(function successCallback(response) {
                //console.log(response.data[0]);

                $('#summernote').summernote('code', response.data[0].job);
                //get file from sendcv

                $http({
                    method: 'POST',
                    url: '/database',
                    "data": {
                        "func": "select",
                        "table": "sendcv",
                        "user": hiddenLoginUser,
                        "jobid": obj._id
                    }
                }).then(function successCallback(response) {
                    let select1 = document.getElementById("inputGroupSelect01");

                   // console.log('no user,', response.data );

                    if (!!response.data ){
                    for (var i = 0; i < select1.children.length; i++) {
                         

                        if (select1.children[i].value == response.data.file1) {
                            document.getElementById("inputGroupSelect01").selectedIndex = i;
                            select1.disabled = true;
                            break;
                        }
                    }
                    let select2 = document.getElementById("inputGroupSelect02");
                    for (var i = 0; i < select2.children.length; i++) {
                        if (select2.children[i].value == response.data.file2) {
                            document.getElementById("inputGroupSelect02").selectedIndex = i;
                            select2.disabled = true;
                            break;
                        }
                    }

                }
                console.log(response.data );

                     if (hiddenLoginRole == 'user' )
                     {

                        if (!!response.data)  {
                    var applyButton = document.getElementById('apply');
                    applyButton.innerText = 'Cancel this application';
                }
                else
                {
                    var applyButton = document.getElementById('apply');
                    applyButton.innerText = 'Apply this job';
                    document.getElementById("inputGroupSelect01").selectedIndex = 0;
                    document.getElementById("inputGroupSelect02").selectedIndex = 0;
                    document.getElementById("inputGroupSelect01").disabled =false;
                    document.getElementById("inputGroupSelect02").disabled =false;
                }
                     }


                }, function errorCallback(response) {});


            }, function errorCallback(response) {
                swal(
                    'Get files list failed! Code:041;Page:Job',
                    'Server internal error',
                    'warning'
                )
            });


        }, 0);
    }


    $scope.lineClick = function (event) {
        let lastSelected = document.getElementsByClassName('selected')[0];
        if (lastSelected) {
            lastSelected.classList.add('line');
            lastSelected.classList.remove('selected');
        }
        let currentObj = event.currentTarget;
        currentObj.classList.add('selected');
        currentObj.classList.remove('line');
        var obj = $filter('filter')($scope.data, {
            _id: currentObj.firstChild.innerText
        }, true)[0];

        var share = document.getElementsByClassName('sharethis-inline-share-buttons')[0];

        //console.log(share);
        if (!!share) {
            share.setAttribute('data-url', 'http://indeep.tk/jobdetail?id=' + obj._id);
            share.setAttribute('data-title', obj.title);
        }


        $http({
            method: 'POST',
            url: '/database',
            "data": {
                "func": "select",
                "table": "job",
                "_id": obj._id
            }
        }).then(function successCallback(response) {
            // console.log(response);
            $('#summernote').summernote('code', response.data[0].job);
            $http({
                method: 'POST',
                url: '/database',
                "data": {
                    "func": "select",
                    "table": "sendcv",
                    "user": hiddenLoginUser,
                    "jobid": obj._id
                }
            }).then(function successCallback(response) {
                let select1 = document.getElementById("inputGroupSelect01");
                let select2 = document.getElementById("inputGroupSelect02");
                select1.disabled = false;
                select2.disabled = false;

                if (response.data != null) {
                    for (var i = 0; i < select1.children.length; i++) {
                        // console.log(select.children[i].value , response.data.file1 );
                        if (select1.children[i].value == response.data.file1) {
                            select1.selectedIndex = i;
                            select1.disabled = true;
                            break;
                        }
                    }
                }

                if (response.data !=null) {
                    for (var i = 0; i < select2.children.length; i++) {
                        if (select2.children[i].value == response.data.file2) {
                            select2.selectedIndex = i;
                            select2.disabled = true;
                            break;
                        }
                    }
                }

                var applyButton = document.getElementById('apply');
                if (select1.disabled == true) {
                    applyButton.innerText = 'Cancel this application';
                } else {
                    applyButton.innerText = 'Apply this job';
                    select1.selectedIndex= 0;
                    select2.selectedIndex= 0;
                    select2.disabled = false;
                }

            }, function errorCallback(response) {});


        }, function errorCallback(response) {
            swal(
                'Get files list failed! Code:041;Page:Job',
                'Server internal error',
                'warning'
            )
        });
    }
});

document.addEventListener('DOMContentLoaded', function () {
    $('#summernote').summernote({

        placeholder: 'Write something...',
        toolbar: false,
        disableDragAndDrop: true

    });
    $('#summernote').summernote('disable');
    document.getElementsByClassName('note-editable')[0].setAttribute('style', 'background-color: white; height: 700px;');
    let hiddenLoginRole = document.getElementById('hiddenLoginRole').value;
    let hiddenLoginUser = document.getElementById('hiddenLoginUser').value;

    console.log(hiddenLoginRole, hiddenLoginUser);
    var applyButton = document.getElementById('apply');
    //let applyButton =angular.element(document.getElementById('apply'));

    var select1 = document.getElementById("inputGroupSelect01");
    var select2 = document.getElementById("inputGroupSelect02");


    if (hiddenLoginRole != 'user' && hiddenLoginRole != 'boss') {
        select1.disabled = true;
        select2.disabled = true;
        applyButton.disabled = false;
        applyButton.setAttribute('data-target', '#modal-container-login');
        applyButton.setAttribute('data-toggle', 'modal');
        select1.disabled = true;
        select2.disabled = true;

    } else if (hiddenLoginRole == 'boss') {
        applyButton.disabled = true;
        applyButton.setAttribute('data-target', '#');
        applyButton.setAttribute('data-toggle', '');
        select1.disabled = true;
        select2.disabled = true;

    } else if (hiddenLoginRole == 'user') {
        select1.disabled = false;
        select2.disabled = false;
        applyButton.disabled = false;
        var applyButton = angular.element(applyButton);


        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "/database",
            "method": "POST",
            "headers": {
                "Content-Type": "application/x-www-form-urlencoded",
                "Cache-Control": "no-cache",
                "Postman-Token": "c19cbb8e-e574-4724-9d90-64c6aba95f5b"
            },
            "data": {
                "func": "select",
                "table": "cv",
                "user": hiddenLoginUser,
                "field": "title"
               // 'name':document.getElementById('name')
            }
        }

        $.ajax(settings).done(function (response) {
            console.log(response);
            console.log(response[0]);

            response.forEach(element => {
                let option = document.createElement("option");
                option.text = element.title;
                option.value = element._id;
                select1.add(option);
            });
            response.forEach(element => {
                let option = document.createElement("option");
                option.text = element.title;
                option.value = element._id;
                select2.add(option);
            });
        });
    }

}, false);