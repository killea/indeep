var app = angular.module('ui.postjob', ['ngAnimate', 'ngSanitize', 'ui.bootstrap']);

app.controller('PaginationDemoCtrl', function ($scope, $http, $log, $filter,$compile) {
    //callback hell detected
    $http({
        method: 'POST',
        url: '/database',
        "data": {
            "func": "select",
            "table": "sendcv",
            "boss":$("#hiddenLoginUser").val()  
        }
        
    }).then(function successCallback(response) {
       // console.log('httpre',response);
        let myarray = [];
        response.data.forEach(element => {
            myarray.push(element.jobid);
        });
        console.log('http-myarray',myarray);

        $http({
            method: 'POST',
            url: '/database',
            "data": {
                "func": "select",
                "table": "job",
                "array":myarray
            }   
        }).then(function successCallback(response) {

            if (response.data.length==0 ) return; 
           

            var cnt=0;
            var tmpHtml= '';
            response.data.forEach(element => {
                //console.log( element.title);
                if (cnt==0)
                {
                    tmpHtml =  "<div  ng-click='joblinkclick($event)' class = 'joblink selecting'  id='" +element._id+"'>"+ element.title  +"</div>";
                }
                else
                {
                    tmpHtml =  "<div ng-click='joblinkclick($event)' class = 'joblink lining'  id='" +element._id+"'>"+ element.title  +"</div>"; 
                }

                $(".tree").append($compile(tmpHtml)($scope));
                cnt =cnt+1;
            });

        $http({
            method: 'POST',
            url: '/database',
            "data": {
                "func": "select",
                "table": "sendcv",
                "jobid":response.data[0]._id
            }
            
        }).then(function successCallback(response) {
             

            let myarray = [];
            response.data.forEach(element => {
              if (element.file1 != '')   myarray.push ( element.file1);
              if (element.file2 != '')  myarray.push ( element.file2);
            });
            console.log(myarray)
        $http({
            method: 'POST',
            url: '/database',
            "data": {
                "func": "select",
                "table": "cv",
                "array":myarray
            }
            
        }).then(function successCallback(response) {

            console.log('last',response.data);
            $scope.data = response.data;
            $scope.itemsPerPage = 10;
            $scope.totalItems = $scope.data.length;
            $scope.currentPage = 1;
            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };
            $scope.pageChanged = function () {
                $log.log('Page changed to: ' + $scope.currentPage);
            };
            $scope.maxSize = 5;

        }, function errorCallback(response) {
            swal(
                'Get files list failed! Code:03010',
                'Server internal error',
                'warning'
            )
        });
        }, function errorCallback(response) {
            swal(
                'Get files list failed! Code:0300',
                'Server internal error',
                'warning'
            )
        });


    
        }, function errorCallback(response) {
            swal(
                'Get files list failed! Code:0301',
                'Server internal error',
                'warning'
            )
        });
    
 

    }, function errorCallback(response) {
        swal(
            'Get files list failed! Code:0302',
            'Server internal error',
            'warning'
        )
    }); 
    /*
    $scope.layoutDone = function (event) {
        console.log('noangular2', document.getElementsByClassName('selected')[0].firstChild.innerText);

    }*/
    $scope.joblinkclick = function($event) {
       // console.log($event.currentTarget);
        console.log($event.currentTarget.id);   

        let currentObj = $event.currentTarget;
        
        let lastSelecting = document.getElementsByClassName('selecting')[0];

        console.log('lastSelecting',lastSelecting);

        if (!!lastSelecting) {
            lastSelecting.classList.add('lining');
            lastSelecting.classList.remove('selecting');
        }

        
        currentObj.classList.add('selecting');
        currentObj.classList.remove('lining');

        $http({
            method: 'POST',
            url: '/database',
            "data": {
                "func": "select",
                "table": "sendcv",
                "jobid":$event.currentTarget.id
            }
            
        }).then(function successCallback(response) {
             

            let myarray = [];
            response.data.forEach(element => {
              if (element.file1 != '')   myarray.push ( element.file1);
              if (element.file2 != '')  myarray.push ( element.file2);
            });
            console.log(myarray)
        $http({
            method: 'POST',
            url: '/database',
            "data": {
                "func": "select",
                "table": "cv",
                "array":myarray
            }
            
        }).then(function successCallback(response) {

            console.log('last_click',response.data);
            $scope.data = response.data;
            $scope.itemsPerPage = 10;
            $scope.totalItems = $scope.data.length;
            $scope.currentPage = 1;
            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };
            $scope.pageChanged = function () {
                $log.log('Page changed to: ' + $scope.currentPage);
            };
            $scope.maxSize = 5;

        }, function errorCallback(response) {
            swal(
                'Get files list failed! Code:03010',
                'Server internal error',
                'warning'
            )
        });
      });
    }


    $scope.linesDone = function (event) {
        let firstLine = angular.element(document.getElementsByClassName('line')[0]);
        if (firstLine) {
            firstLine.removeClass('line');
            firstLine.addClass('selected');
        }
        //this is a hack, wait angular to finish rendering page
        setTimeout(function () {
            let firstLine = document.getElementsByClassName('selected')[0];
            var obj = $filter('filter')($scope.data, {
                _id: firstLine.firstChild.innerText
            }, true)[0];

             $('#summernote').summernote('code', obj.cv);

             console.log( $('.a1').innerText )
        }, 0);
    }

    $scope.lineClick = function (event) {
        let lastSelected = document.getElementsByClassName('selected')[0];

        console.log(lastSelected );

        if (!!lastSelected) {
            lastSelected.classList.add('line');
            lastSelected.classList.remove('selected');
        }

        let currentObj = event.currentTarget;
        currentObj.classList.add('selected');
        currentObj.classList.remove('line');


        var obj = $filter('filter')($scope.data, { _id: currentObj.firstChild.innerText }, true)[0];

        $('#summernote').summernote('code', obj.cv);
    }




});

// loaded function






/*

$(document).ready(function () {
  

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/database",
        "method": "POST",
        "headers": {
            "Content-Type": "application/x-www-form-urlencoded",
            "Cache-Control": "no-cache",
            "Postman-Token": "c19cbb8e-e574-4724-9d90-64c6aba95f5b"
        },
        "data": {
            "func": "select",
            "table": "sendcv",
            "boss": hiddenLoginUser,
           // 'name':document.getElementById('name')
        }
    }
    $.ajax(settings).done(function (response) {
        console.log('the response',response);
        console.log('response0', response[0]);

        let myarray = [];

        response.forEach(element => {
            myarray.push(element.jobid);
        });

        console.log('myarray', myarray);
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/database",
        "method": "POST",
        "headers": {
            "Content-Type": "application/x-www-form-urlencoded",
            "Cache-Control": "no-cache",
            "Postman-Token": "c19cbb8e-e574-4724-9d90-64c6aba95f5b"
        },
        "data": {
            "func": "select",
            "table": "job",
            "array": myarray
           // 'name':document.getElementById('name')
        }
    }
    $.ajax(settings).done(function (response) {
        console.log(response);

 
         response.forEach(element => {
             console.log( element.title);
             $( ".tree" ).append( "<div class = 'joblink' id='" +element._id+"'>"+ element.title  +"</div>" );
         });

    });





    });




});
*/


document.addEventListener('DOMContentLoaded', function () {
    $('#summernote').summernote({
        placeholder: 'Write something...',
        disableDragAndDrop: true,
        height: 700
    });
    $('#summernote').summernote('disable');
    document.getElementsByClassName('note-editable')[0].setAttribute('style', 'background-color: white; height: 700px;');
}, false);
