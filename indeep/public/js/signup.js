
'use strict';
var myApp = angular.module('myApp', []);
myApp.controller('RegistrationController', ['$scope', function ($scope) {
  $scope.register = function () {

    //var md5 = require("crypto-js/md5");
    
    var type = "user";
    if ( $scope.boss == true)
    {
      type = "boss";
    }
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "/signup",
      "method": "POST",
      "headers": {
        "Content-Type": "application/x-www-form-urlencoded",
        "Cache-Control": "no-cache",
        "Postman-Token": "c19cbb8e-e574-4724-9d90-64c6aba95f5b"
      },
      "data": {
        "user": $("#email").val(),
        "password": $("#password").val(),
        "type":$("#typeinput").val(),
      }
    }
    $.ajax(settings).done(function (response) {
      //console.log(response);
      //console.log(response.ret_code);
      if (response.ret_code == 0) {
        swal({
          type: 'success',
          title: 'Sign up Successful',
          showConfirmButton: false,
          timer: 1500
        })
        //location.reload(true); 
        window.location.replace("/sentmail");
      }
      else if (response.ret_code == 1) {
        swal(
          'Sign up failed!',
          'Email has been used!',
          'warning'
        )
      }
      else{
        swal(
          'Sign up failed!',
          'Server internal error, sign up failed!',
          'warning'
        )

      }

    });

  };


}]);



angular.module('myApp').directive('equals', function () {
  return {
    restrict: 'A', // only activate on element attribute
    require: '?ngModel', // get a hold of NgModelController
    link: function (scope, elem, attrs, ngModel) {
      if (!ngModel) return; // do nothing if no ng-model

      // watch own value and re-validate on change
      scope.$watch(attrs.ngModel, function () {
        validate();
      });

      // observe the other value and re-validate on change
      attrs.$observe('equals', function (val) {
        validate();
      });

      var validate = function () {
        // values
        var val1 = ngModel.$viewValue;
        var val2 = attrs.equals;

        // set validity
        ngModel.$setValidity('equals', !val1 || !val2 || val1 === val2);
      };
    }
  }
});


