var app = angular.module('ui.postjob', ['ngAnimate', 'ngSanitize', 'ui.bootstrap']);

app.controller('PaginationDemoCtrl', function ($scope, $http, $log, $filter) {

    $http({
        method: 'POST',
        url: '/database',
        "data": {
            "func": "select",
            "table": "job",
            "user":$("#hiddenLoginUser").val()  
        }
        
    }).then(function successCallback(response) {
        
       // console.log(response.data);
        $scope.data = response.data;
        $scope.itemsPerPage = 10;
        $scope.totalItems = $scope.data.length;
        $scope.currentPage = 1;
        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };
        $scope.pageChanged = function () {
            $log.log('Page changed to: ' + $scope.currentPage);
        };
        $scope.maxSize = 5;

    }, function errorCallback(response) {
        swal(
            'Get files list failed! Code:03',
            'Server internal error',
            'warning'
        )
    });
    /*
    $scope.layoutDone = function (event) {
        console.log('noangular2', document.getElementsByClassName('selected')[0].firstChild.innerText);

    }*/
    $scope.linesDone = function (event) {
        let firstLine = angular.element(document.getElementsByClassName('line')[0]);
        if (firstLine) {
            firstLine.removeClass('line');
            firstLine.addClass('selected');
        }
        //this is a hack, wait angular to finish rendering page
        setTimeout(function () {
            let firstLine = document.getElementsByClassName('selected')[0];
            var obj = $filter('filter')($scope.data, {
                _id: firstLine.firstChild.innerText
            }, true)[0];

            document.getElementById('post').innerText = obj.post?"Revoke":"Post";
            document.getElementById('company').value = obj.company;
            document.getElementById('hremail').value = obj.hremail;
            document.getElementById('city').value = obj.city;
            
           // $("#inputGroupSelect01 :selected").text() = ;

            let select = document.getElementById("inputGroupSelect01");
            for ( var i=0;i<select.children.length;i++)
            {
                //console.log(select.children[i].innerText);
                if ( select.children[i].innerText == obj.jobtype )
                {
                    document.getElementById("inputGroupSelect01").selectedIndex = i;
                    break;
                }
            }



            let selectPro = document.getElementById("inputGroupSelect02");
            for ( var i=0;i<selectPro.children.length;i++)
            {
                //console.log(selectPro.children[i].innerText);
                if ( selectPro.children[i].innerText == obj.province )
                {   

               //     console.log('got it~~~~~~~~~~~~~~~~~~~~~')
                    document.getElementById("inputGroupSelect02").selectedIndex = i;
                    break;
                }
                document.getElementById("inputGroupSelect02").selectedIndex = 13;
            }
        //#endregion
        $http({
            method: 'POST',
            url: '/database',
            "data": {
                "func": "select",
                "table": "job",
                "_id": obj._id
            }
        }).then(function successCallback(response) {
            $('#summernote').summernote('code', response.data[0].job);
        }, function errorCallback(response) {});




        }, 1);
    }

    $scope.lineClick = function (event) {
        let lastSelected = document.getElementsByClassName('selected')[0];
        if (lastSelected) {
            lastSelected.classList.add('line');
            lastSelected.classList.remove('selected');
        }

        let currentObj = event.currentTarget;
        currentObj.classList.add('selected');
        currentObj.classList.remove('line');


        var obj = $filter('filter')($scope.data, { _id: currentObj.firstChild.innerText }, true)[0];

       // console.log(obj);

        document.getElementById('company').value = obj.company;
        document.getElementById('hremail').value = obj.hremail;
        document.getElementById('city').value = obj.city;

        document.getElementById('post').innerText = obj.post?"Revoke":"Post";

        let select = document.getElementById("inputGroupSelect01");
        for ( var i=0;i<select.children.length;i++)
        {
            if ( select.children[i].innerText == obj.jobtype )
            {
                document.getElementById("inputGroupSelect01").selectedIndex = i;
                break;
            }
            
        }


        let selectPro = document.getElementById("inputGroupSelect02");
        for ( var i=0;i<selectPro.children.length;i++)
        {
           // console.log(selectPro.children[i].innerText);
            if ( selectPro.children[i].innerText == obj.province )
            {
                document.getElementById("inputGroupSelect02").selectedIndex = i;
                break;
            }
            document.getElementById("inputGroupSelect02").selectedIndex = 13;
        }
        
        console.log(obj._id);
        $http({
            method: 'POST',
            url: '/database',
            "data": {
                "func": "select",
                "table": "job",
                "_id": obj._id
            }
        }).then(function successCallback(response) {
            $('#summernote').summernote('code', response.data[0].job);
        }, function errorCallback(response) {});

            // console.log(response);



        
    }
    $scope.postFile = function (event) {
        let lastSelected = document.getElementsByClassName('selected')[0];
        var obj = $filter('filter')($scope.data, { _id: lastSelected.firstChild.innerText }, true)[0];
      //  console.log( obj);

        let tmpstr,tmpstr2,tmpword;
        if ( !obj.post)
        {
           tmpstr =  'Post file "' + obj.title + '"?';
           tmpstr2 ="This file be accessed by other users!";
           tmpword ="Post";
        }
        else{
           tmpstr =  'Revoke file "' + obj.title + '"?';
           tmpstr2 ="This file will not be accessed by other users!";
           tmpword ="Revoke";
        }
        swal({
            title:tmpstr,
            text: tmpstr2 ,
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes. ' +tmpword+' it!'
        }).then((result) => {
            if (result.value) {
                $http({
                    method: 'POST',
                    url: '/database',
                    "data": {
                        "func": "update",
                        "table": "job",
                        "_id": lastSelected.firstChild.innerText,
                        "post": !obj.post,
                        "user": $("#hiddenLoginUser").val()

                    }
                }).then(function successCallback(response) {
                    obj.post= !obj.post;
                    if ( obj.post ==true)
                    {   
                        document.getElementById('post').innerText =  "Revoke";
                        swal(
                            'Posted!',
                            'Your file now can be accessed by others.',
                            'success'
                        )
                    }
                    else
                    {
                        document.getElementById('post').innerText =  "Post";
                        swal(
                            'Revoked!',
                            'Your file is revoked,others cannot access anymore.',
                            'success'
                        )
                    }
                    
                }, function errorCallback(response) {
                    swal(
                         'Get files list failed! Code:JOB05delete',
                         'Server internal error',
                         'warning'
                    )
                });
            }
        })




    }






    $scope.renameFile = function () {
        let lastSelected = document.getElementsByClassName('selected')[0];

        if (!lastSelected) {
            return;
        }
        //console.log( 'rename',lastSelected.firstChild.nextSibling.innerText);
        swal({
            title: 'Input new file name',
            input: 'text',
            type: 'info',
            text: 'Rename the file',
            showCancelButton: true,
            inputValue: lastSelected.firstChild.nextSibling.innerText,
            inputAttributes: {
                autocapitalize: 'off'
            }
        }).then((title) => {

            //console.log(title.value);
            if (title.value === undefined) {
                return;
            } else if ($.trim(title.value).length === 0) {
                title.value = 'New file';
            }

            var obj = $filter('filter')($scope.data, {
                _id: lastSelected.firstChild.innerText
            }, true)[0];
            obj.title = title.value;

            lastSelected.firstChild.nextSibling.innerText = title.value;

            $http({
                method: 'POST',
                url: '/database',
                "data": {
                    "func": "update",
                    "table": "job",
                    "title": title.value,
                    "_id": lastSelected.firstChild.innerText,
                    "user": $("#hiddenLoginUser").val()
                }
            }).then(function successCallback(response) {

                swal(
                    'Rename file success!',
                    'Good job!',
                    'info'
                )
            }, function errorCallback(response) {
                swal(
                    'Get files list failed! Code:02Rename',
                    'Server internal error',
                    'warning'
                )
            });
        });



    }
    $scope.saveFile = function () {
        let lastSelected = document.getElementsByClassName('selected')[0];

        if (!lastSelected) {
            return;
        }


        
        var obj = $filter('filter')($scope.data, {
            _id: lastSelected.firstChild.innerText
        }, true)[0];


        obj.job = $('#summernote').summernote('code');
        obj.company = $('#company').val();
        obj.hremail = $('#hremail').val();
        obj.jobtype = $("#inputGroupSelect01 :selected").text();
        obj.province  = $("#inputGroupSelect02 :selected").text();
        obj.city = $('#city').val();
        //console.log($("#inputGroupSelect01 :selected").text());
        $http({
            method: 'POST',
            url: '/database',
            "data": {
                "func": "update",
                "table": "job",
                "company": $('#company').val(),
                "hremail": $('#hremail').val(),
                "jobtype": $("#inputGroupSelect01 :selected").text(),
                "job": $('#summernote').summernote('code'),
                "_id": lastSelected.firstChild.innerText,
                "user": $("#hiddenLoginUser").val()
            }
        }).then(function successCallback(response) {
            swal(
                'Save file success!',
                'Good job!',
                'info'
            )
        }, function errorCallback(response) {
            swal(
                'Save files failed!',
                'Server internal error',
                'warning'
            )
        });
    }

    $scope.deleteFile = function () {

        let lastSelected = document.getElementsByClassName('selected')[0];
        if (!lastSelected) {
            return;
        }

        let jobtitle = lastSelected.firstChild.nextSibling.innerText;


        swal({
            title: 'Delete "' + jobtitle + '"?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {

            if (result.value) {

                $http({
                    method: 'POST',
                    url: '/database',
                    "data": {
                        "func": "delete",
                        "table": "job",
                        //"title": title.value,
                        "_id": lastSelected.firstChild.innerText,
                        "user": $("#hiddenLoginUser").val()
                    }
                }).then(function successCallback(response) {

                    $http({
                        method: 'POST',
                        url: '/database',
                        "data": {
                            "func": "select",
                            "table": "job",
                            "user": $("#hiddenLoginUser").val()
                        }
                    }).then(function successCallback(response) {
                        //console.log(response.data);
                        $scope.data = response.data;
                        $scope.totalItems = $scope.data.length;
                        $scope.currentPage = 1;

                        swal(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                        )

                    }, function errorCallback(response) {
                        swal(
                            'Get files list failed! Code:JOB03delete',
                            'Server internal error',
                            'warning'
                        )
                    });


                }, function errorCallback(response) {
                    swal(
                        'Get files list failed! Code:JOB05delete',
                        'Server internal error',
                        'warning'
                    )
                });
            }
        })

    }

    $scope.newFile = function () {
        swal({
            title: 'Input job post file name',
            input: 'text',
            type: 'info',
            text: 'Create a new file',
            showCancelButton: true,
            inputAttributes: {
                autocapitalize: 'off'
            }
        }).then((title) => {
            if (title.value === undefined) {
                return;
            } else if ($.trim(title.value).length === 0) {
                title.value = 'New file';
            }

            $http({
                method: 'POST',
                url: '/database',
                "data": {
                    "func": "insert",
                    "table": "job",
                    "title": title.value,
                    "user": $("#hiddenLoginUser").val(),
                }
            }).then(function successCallback(response) {
                $http({
                    method: 'POST',
                    url: '/database',
                    "data": {
                        "func": "select",
                        "table": "job",
                        "user": $("#hiddenLoginUser").val()
                    }
                }).then(function successCallback(response) {
                    $scope.data = response.data;
                    $scope.totalItems = $scope.data.length;
                    $scope.currentPage = 1;
                    $scope.setPage = function (pageNo) {
                        $scope.currentPage = pageNo;
                    };
                    $scope.pageChanged = function () {
                        $log.log('Page changed to: ' + $scope.currentPage);
                    };
                    $scope.maxSize = 5;
                }, function errorCallback(response) {
                    swal(
                        'Get files list failed! Code:JOB01',
                        'Server internal error',
                        'warning'
                    )
                });

            }, function errorCallback(response) {
                swal(
                    'Get files list failed! Code:JOB02',
                    'Server internal error',
                    'warning'
                )
            });



        });


    };
});






// loaded function
document.addEventListener('DOMContentLoaded', function () {
    $('#summernote').summernote({
        placeholder: 'Write something...',
        height: 600
    });
}, false);



/*
window.onload = function () {

};
$(document).ready(function () {

});
*/