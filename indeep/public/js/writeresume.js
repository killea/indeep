var app = angular.module('ui.bootstrap.demo', ['ngAnimate', 'ngSanitize', 'ui.bootstrap']);

/*
app.directive('repeatDone', function() {
    return function(scope, element, attrs) {
      element.bind('$destroy', function(event) {
        if (scope.$last) {
          scope.$eval(attrs.repeatDone);
        }
      });
    };
  });
  */
app.controller('PaginationDemoCtrl', function ($scope, $http, $log, $filter) {
    $http({
        method: 'POST',
        url: '/database',
        "data": {
            "func": "select",
            "table": "cv",
            "user": $("#hiddenLoginUser").val()
        }
    }).then(function successCallback(response) {
        //console.log(response.data);
        $scope.data = response.data;
        $scope.itemsPerPage = 15;
        $scope.totalItems = $scope.data.length;

        $scope.currentPage = 1;

        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };
        $scope.pageChanged = function () {
            $log.log('Page changed to: ' + $scope.currentPage);
        };
        $scope.maxSize = 5;

    }, function errorCallback(response) {
        swal(
            'Get files list failed! Code:03',
            'Server internal error',
            'warning'
        )
    });
    /*
    $scope.layoutDone = function (event) {
        console.log('noangular2', document.getElementsByClassName('selected')[0].firstChild.innerText);

    }*/
    $scope.linesDone = function (event) {


        let firstLine = angular.element(document.getElementsByClassName('line')[0]);

        if (firstLine) {
            firstLine.removeClass('line');
            firstLine.addClass('selected');
        }

        //this is a hack, wait angular to finish rendering page
        setTimeout(function () {
            let firstLine = document.getElementsByClassName('selected')[0];
            var obj = $filter('filter')($scope.data, {
                _id: firstLine.firstChild.innerText
            }, true)[0];
            $('#summernote').summernote('code', obj.cv);

        }, 0);
    }

    $scope.lineClick = function (event) {
        let lastSelected = document.getElementsByClassName('selected')[0];
        if (lastSelected) {
            lastSelected.classList.add('line');
            lastSelected.classList.remove('selected');
        }

        let currentObj = event.currentTarget;
        currentObj.classList.add('selected');
        currentObj.classList.remove('line');

        //console.log(currentObj.firstChild.innerText);
        var obj = $filter('filter')($scope.data, {
            _id: currentObj.firstChild.innerText
        }, true)[0];
        //console.log(obj);
        $('#summernote').summernote('code', obj.cv);

    }


    $scope.deleteFile = function () {
        let lastSelected = document.getElementsByClassName('selected')[0];

        if (!lastSelected) {
            return;
        }


    }
    $scope.renameFile = function () {
        let lastSelected = document.getElementsByClassName('selected')[0];

        if (!lastSelected) {
            return;
        }
        //console.log( 'rename',lastSelected.firstChild.nextSibling.innerText);
        swal({
            title: 'Input new file name',
            input: 'text',
            type: 'info',
            text: 'Rename the file',
            showCancelButton: true,
            inputValue: lastSelected.firstChild.nextSibling.innerText,
            inputAttributes: {
                autocapitalize: 'off'
            }
        }).then((title) => {

            //console.log(title.value);
            if (title.value === undefined) {
                return;
            } else if ($.trim(title.value).length === 0) {
                title.value = 'New file';
            }

            var obj = $filter('filter')($scope.data, {
                _id: lastSelected.firstChild.innerText
            }, true)[0];
            obj.title = title.value;

            lastSelected.firstChild.nextSibling.innerText = title.value;

            $http({
                method: 'POST',
                url: '/database',
                "data": {
                    "func": "update",
                    "table": "cv",
                    "title": title.value,
                    "_id": lastSelected.firstChild.innerText,
                    "user": $("#hiddenLoginUser").val()
                }
            }).then(function successCallback(response) {

                swal(
                    'Rename file success!',
                    'Good job!',
                    'info'
                )
            }, function errorCallback(response) {
                swal(
                    'Get files list failed! Code:02Rename',
                    'Server internal error',
                    'warning'
                )
            });
        });


    }
    $scope.saveFile = function () {
        let lastSelected = document.getElementsByClassName('selected')[0];

        if (!lastSelected) {
            return;
        }

        var obj = $filter('filter')($scope.data, {
            _id: lastSelected.firstChild.innerText
        }, true)[0];
        obj.cv = $('#summernote').summernote('code');
        $http({
            method: 'POST',
            url: '/database',
            "data": {
                "func": "update",
                "table": "cv",
                "cv": $('#summernote').summernote('code'),
                "_id": lastSelected.firstChild.innerText,
                "user": $("#hiddenLoginUser").val()
            }
        }).then(function successCallback(response) {
            swal(
                'Save file success!',
                'Good job!',
                'info'
            )
        }, function errorCallback(response) {
            swal(
                'Save files failed!',
                'Server internal error',
                'warning'
            )
        });
    }

    $scope.deleteFile = function () {

        let lastSelected = document.getElementsByClassName('selected')[0];
        if (!lastSelected) {
            return;
        }

        let cvtitle = lastSelected.firstChild.nextSibling.innerText;


        swal({
            title: 'Delete "' + cvtitle + '"?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {

            if (result.value) {

                $http({
                    method: 'POST',
                    url: '/database',
                    "data": {
                        "func": "delete",
                        "table": "cv",
                        //"title": title.value,
                        "_id": lastSelected.firstChild.innerText,
                        "user": $("#hiddenLoginUser").val()
                    }
                }).then(function successCallback(response) {

                    $http({
                        method: 'POST',
                        url: '/database',
                        "data": {
                            "func": "select",
                            "table": "cv",
                            "user": $("#hiddenLoginUser").val()
                        }
                    }).then(function successCallback(response) {
                        //console.log(response.data);
                        $scope.data = response.data;
                        $scope.totalItems = $scope.data.length;
                        $scope.currentPage = 1;

                        swal(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                        )

                    }, function errorCallback(response) {
                        swal(
                            'Get files list failed! Code:03delete',
                            'Server internal error',
                            'warning'
                        )
                    });


                }, function errorCallback(response) {
                    swal(
                        'Get files list failed! Code:05delete',
                        'Server internal error',
                        'warning'
                    )
                });
            }
        })

    }

    $scope.newFile = function () {
        swal({
            title: 'Input new file name',
            input: 'text',
            type: 'info',
            text: 'Create a new file',
            showCancelButton: true,
            inputAttributes: {
                autocapitalize: 'off'
            }
        }).then((title) => {


            if (title.value === undefined) {
                return;
            } else if ($.trim(title.value).length === 0) {
                title.value = 'New file';
            }

            $http({
                method: 'POST',
                url: '/database',
                "data": {
                    "func": "insert",
                    "table": "cv",
                    "title": title.value,
                    "user": $("#hiddenLoginUser").val()
                }
            }).then(function successCallback(response) {
                $http({
                    method: 'POST',
                    url: '/database',
                    "data": {
                        "func": "select",
                        "table": "cv",
                        "user": $("#hiddenLoginUser").val()
                    }
                }).then(function successCallback(response) {
                    $scope.data = response.data;
                    // $scope.itemsPerPage = 10;
                    $scope.totalItems = $scope.data.length;

                    $scope.currentPage = 1;

                    $scope.setPage = function (pageNo) {
                        $scope.currentPage = pageNo;
                    };
                    $scope.pageChanged = function () {
                        $log.log('Page changed to: ' + $scope.currentPage);
                    };
                    $scope.maxSize = 5;
                }, function errorCallback(response) {
                    swal(
                        'Get files list failed! Code:01',
                        'Server internal error',
                        'warning'
                    )
                });

            }, function errorCallback(response) {
                swal(
                    'Get files list failed! Code:02',
                    'Server internal error',
                    'warning'
                )
            });



        });


    };
});






// loaded function
document.addEventListener('DOMContentLoaded', function () {
    $('#summernote').summernote({
        placeholder: 'Write something...',
        height: 600
    });
}, false);



/*
window.onload = function () {

};
$(document).ready(function () {

});
*/