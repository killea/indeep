

document.querySelector('#choose').addEventListener('click', function() {
	document.querySelector('#file').click();
});



document.querySelector('#changepassword').addEventListener('click', function() {
    let pass1 = document.getElementById('pass1').value;
    let pass2 = document.getElementById('pass2').value;
    let pass3 = document.getElementById('pass3').value;
    if ( pass2 != pass3)
    {
        swal(
            "Password doesn't match",
            'please check the new password',
            'warning'
        )
        return;
    }
    let settings = {
        "async": true,
        "crossDomain": true,
        "url": "/changepassword",
        "method": "POST",
        "headers": {
            "Content-Type": "application/x-www-form-urlencoded",
            "Cache-Control": "no-cache"
        },
        "data": {
            "user": hiddenLoginUser,
            "oldpasstext":md5(pass1),
            "passtext":md5(pass2)
        }
    }
    $.ajax(settings).done(function (response) {
       // console.log(response);
        if ( response.ret_code ==0)
        {
        swal(
            'Password changed!',
            'Good job',
            'info'
        )
        document.getElementById('pass1').value='';
        document.getElementById('pass2').value='';
        document.getElementById('pass3').value='';
       // window.location.replace ('/');
        }
        else
        {
            swal(
                'Password changing failed!',
                'Server internal error',
                'warning'
            )
        }
    });
   
});

document.querySelector('#pass1').addEventListener('change', function() {
    console.log(md5( document.querySelector('#pass1').value));
    let userName =document.getElementById('hiddenLoginUser').value;

    console.log(userName,md5( document.querySelector('#pass1').value)  );
    
});

document.querySelector('#pass2').addEventListener('input', function() {
    console.log(document.querySelector('#pass2').value );
    if (document.querySelector('#pass2').value == document.querySelector('#pass3').value  )
    {$("#message").hide();}
    if (document.querySelector('#pass2').value != document.querySelector('#pass3').value  )
    {$("#message").show();}
});

document.querySelector('#pass3').addEventListener('input', function() {
    console.log(document.querySelector('#pass2').value );
    if (document.querySelector('#pass2').value == document.querySelector('#pass3').value  )
    {$("#message").hide();}
    if (document.querySelector('#pass2').value != document.querySelector('#pass3').value  )
    {$("#message").show();}
    
});


$(document).ready(function () {
    var ctx = document.getElementById('myCanvas').getContext('2d');
    var img = new Image;
    img.onload = function() {
        ctx.drawImage(img, 0,0, 100 , 100);
        
    }
    img.src = document.getElementById('avatarIcon').src ;

    {$("#message").hide();}
});

//$("#add_fields_placeholderValue").hide();


document.querySelector('#file').addEventListener('change', function() {
	var file = this.files[0];
	console.log(file);
	//document.querySelector('#error-message').style.display = 'none';
    // Validate MIME type
    if ( !file)
    {return};
    // console.log( file.type, 'file type');


	if(file.type.indexOf('image/') == -1) {
        swal(
            'Only picture files allowed',
            'File is not a correct picture',
            'warning'
        )
		return;
	}

	// Max  10Mb allowed
	if(file.size > 10*1024*1024) {
		//document.querySelector('#error-message').style.display = 'block';
        //document.querySelector('#error-message').innerText = 'Error : Exceeded size 2MB';
        swal(
            'File is too big',
            'File is bigger than 10m',
            'warning'
        )
		return;
    }
    var ctx = document.getElementById('myCanvas').getContext('2d');
var img = new Image;
img.onload = function() {
    ctx.clearRect(0, 0, document.getElementById('myCanvas').width, document.getElementById('myCanvas').height);
    ctx.drawImage(img, 0,0, 100 , 100);
}
img.src = URL.createObjectURL(file);

    //$("#avatar").attr("src",file);
	//document.querySelector('#file').innerText = file.name;
});
$('#submit').click(function (event) {
    function dataURItoBlob(dataURI) {
        // convert base64/URLEncoded data component to raw binary data held in a string
        var byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0)
            byteString = atob(dataURI.split(',')[1]);
        else
            byteString = unescape(dataURI.split(',')[1]);
        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        // write the bytes of the string to a typed array
        var ia = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }
        return new Blob([ia], {type:mimeString});
     }
    var snap = document.getElementById('myCanvas');
    var myimg  = snap.toDataURL();
    var myfile = dataURItoBlob(myimg);
    console.log(myfile);
    //#endregion
    // https://stackoverflow.com/questions/19032406/convert-html5-canvas-into-file-to-be-uploaded

    var formData = new FormData();
    //formData.append('file', $('#file')[0].files[0]);
    formData.append('file',myfile,'test.png');


    $.ajax({
        url: '/upload',
        type: 'POST',
        cache: false,
        data: formData,
        processData: false,
        contentType: false
    }).done(function (res) {
        if (res.ret_code == 0){

            document.getElementById('avatarIcon').src="./upload/" + res.ret_path +".png";
            console.log(res.ret_path); 
            swal(
                'Update avatar success',
                'Good job',
                'info'
            )
    
        }
        else
        {
            swal(
                'Update avatar failed',
                'Server error 01',
                'warning'
            )    
        }
    }).fail(function (res) {
        swal(
            'Update avatar failed',
            'Server error 02',
            'warning'
        )
     });



});