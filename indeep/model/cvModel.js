const mongoose = require('mongoose');
var Schema = mongoose.Schema;
var cvSchema = new Schema(
  {
    user: {
      type: String
    },
    title: {
      type: String
    },
    cv: {
      type: String
    },
    createDate: {
      type: Date
    }
  },
  {
    collection: "cv"
  }
);

var cvModel = mongoose.model("cv", cvSchema);
module.exports = cvModel;
