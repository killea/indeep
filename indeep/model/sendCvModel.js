const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var sendCvSchema = new Schema(
    {
      boss: {
        type: String
      }
    },
    {
      collection: "sendcv"
    }
  );
  var sendCvModel = mongoose.model("sendcv", sendCvSchema);
  module.exports = sendCvModel;