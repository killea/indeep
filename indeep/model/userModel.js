const mongoose = require('mongoose');

var Schema = mongoose.Schema;
const UserSchema = new Schema(
  {
    user: String,
    password: String,
    type: String,
    status: String,
    signup_datetime: Date,
    facebook: String,
    twitter: String,
    google: String,
    pix:String
  },
  {
    collection: "user"
  }
);
var User = mongoose.model("user", UserSchema);
module.exports = User;