const mongoose = require('mongoose');

var Schema = mongoose.Schema;
var jobSchema = new Schema(
  {
    user: {
      type: String
    },
    title: {
      type: String
    },
    company: {
      type: String
    },
    hremail: {
      type: String
    },
    job: {
      type: String
    }, //
    jobtype: {
      type: String
    },
    city: {
      type: String
    },
    province: {
      type: String
    },
    createDate: {
      type: Date
    },
    post: {
      tpye: Boolean
    }
  },
  {
    collection: "job"
  }
);
var jobModel = mongoose.model("job", jobSchema);
module.exports = jobModel;
