//var router  = require('./index');
"use strict";
var mongoose = require('mongoose').set('debug', true);
var filterStr = require("./helper.js");
//var url = require("url");
var md5 = require("crypto-js/md5");
var ObjectID = require("mongodb").ObjectID;
var send = require("./mail.js");

var cvModel = require("../model/cvModel.js");
var jobModel = require("../model/jobModel.js");
var User = require("../model/userModel.js");
var sendCvModel = require("../model/sendCvModel.js");
const myuser = require('./myuser');


var fs = require('fs');
var ini = require('ini');
var config = ini.parse(fs.readFileSync('../indeep/config/config.ini', 'utf-8'));
var myUrl =config.setting.myurl;

exports.postSetRole = async (req, res, next) => { 
    console.log(req.body);
    if ( req.body.way ==undefined)
    {
     req.body.way ='';
    }
    var google='';
    var facebook ='';
    var twitter ='';
    if (  req.body.way =='google')
    {
     google = req.body.user.toLowerCase();
    }
    else if (  req.body.way =='facebook')
    {
     facebook = req.body.user.toLowerCase();
    }
    else if (  req.body.way =='twitter' )
    {
     twitter=   req.body.user.toLowerCase();
    }
   
    var myid = new ObjectID();
   
    req.session.loginRole = req.body.role;
   
    var user = {
      _id: myid,
      user: req.body.user.toLowerCase(),
      password: '',
      type: req.body.role,
      status: "active",
      google:google,
      facebook:facebook,
      twitter:twitter,
      signup_datetime: new Date()
    };
   
    var conn = mongoose.connection;

    console.log( 'connection:',conn);
    try {
      conn.collection("user").insert(user);
    } catch (error) {
      console.log(error);
      res.json({
        ret_code: 2,
        ret_msg: "Database error!"
      });
      return;
    }
   
    res.json({
     ret_code: 0,
     ret_msg: "Created user google!"
   });

   };
  

exports.postDatabase = (req,res , next ) => {
   //router.post("/database", function(req, res, next) {
    console.log("user", req.session.loginUser);
    let func = req.body.func; // name
    let table = req.body.table;
    //console.log(func);
    switch (func) {
      case "insert":

      /*
        if (req.body.user != myuser.loginUser || !req.body.user) {
          console.log("2 users", req.body.user, myuser.loginUser);
          res.json({
            ret_code: 2,
            ret_msg: "Insert request denied!"
          });
          return;
        }*/
  
        if (table == "cv") {
          var cv = {
            user: req.body.user,
            title: req.body.title,
            cv: "",
            createDate: new Date()
          };
          var conn = mongoose.connection;
          try {
            conn.collection("cv").insert(cv);
          } catch (error) {
            console.log(error);
            res.json({
              ret_code: 2,
              ret_msg: "Database error!"
            });
            return;
          }
          res.json({
            ret_code: 0,
            ret_msg: "insert ok!"
          });
          /*
          cvModel.find({ 'user': req.body.user }, null, { sort: { createDate: -1 } }, function (err, docs) {
            console.log(docs);
          });*/
          return;
        } else if (table == "job") {
          var job = {
            user: req.body.user,
            title: req.body.title,
            job: "",
            company: "",
            hremail: "",
            jobtype: "",
            post: false,
            city: "",
            province: "",
            createDate: new Date()
          };
  
          var conn = mongoose.connection;
          try {
            conn.collection("job").insert(job);
          } catch (error) {
            console.log(error);
            res.json({
              ret_code: 2,
              ret_msg: "Database error!"
            });
            return;
          }
          res.json({
            ret_code: 0,
            ret_msg: "insert ok!"
          });
          /*
          cvModel.find({ 'user': req.body.user }, null, { sort: { createDate: -1 } }, function (err, docs) {
            console.log(docs);
          });*/
          return;
        } else if (table == "sendcv") {
          var sendcv = {
            user: req.body.user,
            file1: req.body.file1,
            file2: req.body.file2,
            boss: req.body.boss,
            jobid: req.body.jobid
          };
  
          var conn = mongoose.connection;
          try {
            conn.collection("sendcv").insert(sendcv);
          } catch (error) {
            console.log(error);
            res.json({
              ret_code: 2,
              ret_msg: "Database error!"
            });
            return;
          }
          res.json({
            ret_code: 0,
            ret_msg: "insert ok!"
          });
        }
        break;
      case "select":
        if (table == "cv") {
          if (!!req.body.array) {
            console.log("array", req.body.array);
            let tmp = [];
            req.body.array.forEach(element => {
              tmp.push(mongoose.Types.ObjectId(element));
            });
            cvModel.find(
              {
                _id: {
                  $in: tmp
                }
              },
              null,
              null,
              function(err, docs) {
                res.json(docs);
              }
            );
  
            return;
          }
  
          if (req.body.field == "title") {
            cvModel.find(
              {
                user: req.body.user
              },
              {
                title: ""
              },
              {
                sort: {
                  createDate: -1
                }
              },
              function(err, docs) {
                res.json(docs);
              }
            );
          } else {
            cvModel.find(
              {
                user: req.body.user
              },
              null,
              {
                sort: {
                  createDate: -1
                }
              },
              function(err, docs) {
                res.json(docs);
              }
            );
          }
        } else if (table == "sendcv") {
          if (!!req.body.user) {
            var sendcv = {
              user: req.body.user,
              jobid: req.body.jobid
            };
  
            var conn = mongoose.connection;
            try {
              conn.collection("sendcv").findOne(sendcv, function(err, docs) {
                res.json(docs);
              });
            } catch (error) {
              console.log(error);
              res.json({
                ret_code: 2,
                ret_msg: "Database error!"
              });
              return;
            }
          } else if (!!req.body.boss) {
            sendCvModel.find(
              {
                boss: req.body.boss
              },
              null,
              null,
              function(err, docs) {
                res.json(docs);
              }
            );
          } else if (!!req.body.jobid) {
            //mongoose.Types.ObjectId(element )
            sendCvModel.find(
              {
                jobid: req.body.jobid
              },
              null,
              {
                user: 1
              },
              function(err, docs) {
                res.json(docs);
              }
            );
          }
        } else if (table == "job") {
          if (!!req.body.array) {
            console.log("array", req.body.array);
            let tmp = [];
            req.body.array.forEach(element => {
              tmp.push(mongoose.Types.ObjectId(element));
            });
            jobModel.find(
              {
                _id: {
                  $in: tmp
                }
              },
              null,
              null,
              function(err, docs) {
                res.json(docs);
              }
            );
  
            return;
          }
  
          let user = req.body.user;
          let _id = req.body._id;
          if (!!user) {
            jobModel.find(
              {
                user: user
              },
              {
                job: 0
              },
              {
                sort: {
                  createDate: -1
                }
              },
              function(err, docs) {
                res.json(docs);
              }
            );
          } else if (!!_id) {
            jobModel.find(
              {
                _id: _id
              },
              null,
              null,
              function(err, docs) {
                //  console.log('doc------------------------',docs);
                res.send(docs);
              }
            );
          } else {
            //get the jobs by keywords and places
            let keywords = filterStr(req.body.what.toLowerCase());
            let places = filterStr(req.body.where.toLowerCase());
            if (keywords == "" && places == "")
              jobModel.find(
                {
                  post:true
                },
                {
                  job: 0
                },
                {
                  sort: {
                    createDate: -1
                  }
                },
                function(err, docs) {
                  res.json(docs);
                }
              );
            else if (keywords == "" && places != "") {
              let query =
                `{"$where": "function() 
              { if (this.post==false) {return false;};
                if (this.job==null) {return false;};
                let mycity = (this.city == null) ? '':this.city.toLowerCase();
              let myprovince =  (this.province == null)? '': this.province.toLowerCase();
              let myplaces = '` +
                places +
                `';
              let places = myplaces.split(' ');
              if ( places.includes(mycity) || places.includes(myprovince))
              {
              return true;
              }
              else
              {
              return false;
              }
              }"
              }`;
              jobModel.find(
                JSON.parse(query.replace(/[\r\n]/g, "")),
                {
                  job: 0
                },
                {
                  sort: {
                    createDate: -1
                  }
                },
                function(err, docs) {
                  res.json(docs);
                }
              );
            } else if (keywords != "" && places == "") {
              let query =
                `{"$where": "function() 
              {if (this.post==false) {return false;};
                if (this.job==null) {return false;}
                let mykeywords='` +
                keywords +
                `';
              let keys = mykeywords.split(' ');
                var cnt =0;
                const maxKeyNum=10;
                for ( i=0;i<maxKeyNum;i++)
                {
                  if (i>= keys.length)
                  {
                    break;
                  }
                  if ( this.job.indexOf( keys[i] )<0)
                  {
                    return false;
                  }
                  else{
                    cnt=cnt+1;
                  }
                }
                if (cnt==maxKeyNum || cnt== keys.length)
                {
                  return true;
                }
                else
                {return false;}
              }"
              }`;
              jobModel.find(
                JSON.parse(query.replace(/[\r\n]/g, "")),
                {
                  job: 0
                },
                {
                  sort: {
                    createDate: -1
                  }
                },
                function(err, docs) {
                  res.json(docs);
                }
              );
            } else if (keywords != "" && places != "") {
              //These js in query will run on mongodb server.
              let query =
                `{"$where": "function() 
              {
              if (this.post==false) {return false;};
              if (this.job==null) {return false;};
              let mycity = (this.city == null) ? '':this.city.toLowerCase();
              let myprovince =  (this.province == null)? '': this.province.toLowerCase();
              let myplaces = '` +
                places +
                `';let mykeywords='` +
                keywords +
                `';
              let places = myplaces.split(' ');
              let keys = mykeywords.split(' ');
              if ( places.includes(mycity) || places.includes(myprovince))
              {
                var cnt =0;
                const maxKeyNum=10;
  
                var wholestr = (this.job + ' '+this.title).toLowerCase();
                for ( i=0;i<maxKeyNum;i++)
                {
                  if (i>= keys.length)
                  {
                    break;
                  }
                  if ( this.job.indexOf( keys[i] )<0)
                  {
                    return false;
                  }
                  else{
                    cnt=cnt+1;
                  }
                }
                if (cnt==maxKeyNum || cnt== keys.length)
                {return true;}
                else
                {return false;}
              }
              else
              {
              return false;
              }
              }"
              }`;
              jobModel.find(
                JSON.parse(query.replace(/[\r\n]/g, "")),
                {
                  job: 0
                },
                {
                  sort: {
                    createDate: -1
                  }
                },
                function(err, docs) {
                  res.json(docs);
                }
              );
            }
          }
        }
  
        break;
      case "update":
        if (table == "cv") {
          if (req.body.cv === undefined) {
            cvModel.findOneAndUpdate(
              {
                user: req.body.user,
                _id: mongoose.Types.ObjectId(req.body._id)
              },
              {
                title: req.body.title
              },
              function(err, docs) {
                if (docs) {
                  res.json({
                    ret_code: 0,
                    ret_msg: "File Saved!"
                  });
                } else {
                  res.json({
                    ret_code: 1,
                    ret_msg: "Could not find file!"
                  });
                }
              }
            );
          } else {
            cvModel.findOneAndUpdate(
              {
                user: req.body.user,
                _id: mongoose.Types.ObjectId(req.body._id)
              },
              {
                cv: req.body.cv
              },
              function(err, docs) {
                if (docs) {
                  res.json({
                    ret_code: 0,
                    ret_msg: "File Saved!"
                  });
                } else {
                  res.json({
                    ret_code: 1,
                    ret_msg: "Could not find file!"
                  });
                }
              }
            );
          }
        } else if (table == "job") {
          if (req.body.job === undefined) {
            if (req.body.post !== undefined) {
              jobModel.findOneAndUpdate(
                {
                  user: req.body.user,
                  _id: mongoose.Types.ObjectId(req.body._id)
                },
                {
                  post: req.body.post
                },
                function(err, docs) {
                  if (docs) {
                    res.json({
                      ret_code: 0,
                      ret_msg: "File Saved!"
                    });
                  } else {
                    res.json({
                      ret_code: 1,
                      ret_msg: "Could not find file!"
                    });
                  }
                }
              );
            } else {
              jobModel.findOneAndUpdate(
                {
                  user: req.body.user,
                  _id: mongoose.Types.ObjectId(req.body._id)
                },
                {
                  title: req.body.title
                },
                function(err, docs) {
                  if (docs) {
                    res.json({
                      ret_code: 0,
                      ret_msg: "File Saved!"
                    });
                  } else {
                    res.json({
                      ret_code: 1,
                      ret_msg: "Could not find file!"
                    });
                  }
                }
              );
            }
          } else {
            jobModel.findOneAndUpdate(
              {
                user: req.body.user,
                _id: mongoose.Types.ObjectId(req.body._id)
              },
              {
                job: req.body.job,
                company: req.body.company,
                hremail: req.body.hremail,
                jobtype: req.body.jobtype,
                city: req.body.city,
                province: req.body.province
              },
              function(err, docs) {
                if (docs) {
                  //console.log(mongoose.Types.ObjectId(req.body._id));
                  res.json({
                    ret_code: 0,
                    ret_msg: "File Saved!"
                  });
                } else {
                  res.json({
                    ret_code: 1,
                    ret_msg: "Could not find file!"
                  });
                }
              }
            );
          }
        }
  
        break;
      case "delete":
        var theModel;
        if (table == "cv" || table == "job") {
          if (table == "cv") {
            theModel = cvModel;
          } else if (table == "job") {
            theModel = jobModel;
          }
  
          theModel.findOneAndDelete(
            {
              user: req.body.user,
              _id: mongoose.Types.ObjectId(req.body._id)
            },
            function(err, docs) {
              if (docs) {
                res.json({
                  ret_code: 0,
                  ret_msg: "Deleted!"
                });
              } else {
                console.log("delete failed,no doc found");
  
                res.json({
                  ret_code: 1,
                  ret_msg: "Could not find file!"
                });
              }
            }
          );
        } else if (table == "sendcv") {
          var sendcv = {
            file1: req.body.file1,
            file2: req.body.file2,
            user: req.body.user,
            boss: req.body.boss,
            jobid: req.body.jobid
          };
          var conn = mongoose.connection;
          try {
            conn.collection("sendcv").deleteOne(sendcv);
          } catch (error) {
            console.log(error);
            res.json({
              ret_code: 2,
              ret_msg: "Database error!"
            });
            return;
          }
          res.json({
            ret_code: 0,
            ret_msg: "delete ok!"
          });
        }
  
        break;
  
      default:
        console.log("databse function called, but cannot recgonize");
        console.log(table, func);
        res.json({
          ret_code: 2,
          ret_msg: "database function call with wrong parameters!"
        });
    }
  };



exports.postSignup = async ( req, res, next)=> {
  //router.post( "/signup", asyncHandler(async function(req, res, next) {
    
      var finduser = await User.findOne(
        {
          user: req.body.user
        },
        function(err, docs) {
          
        }
      );
  
      if (finduser) {
        res.json({
          ret_code: 1,
          ret_msg: "Email already used!"
        });
        return;
      } else {
        console.log(req.body.user, "not exist!can be signed up");
      }
  
      
  
      var myid = new ObjectID();


      console.log(myid);

      var user = {
        _id: myid,
        user: req.body.user.toLowerCase(),
        password: md5(req.body.password).toString(),
        type: req.body.type,
        status: "null",
        signup_datetime: new Date()
      };
    
      console.log(user);

      var conn = mongoose.connection;
      console.log(conn);


      try {
        conn.collection("user").insert(user);
      } catch (error) {
        console.log(error);
        res.json({
          ret_code: 2,
          ret_msg: "Database error!"
        });
        return;
      }
  
      var part1 = `
    <table class="body" style="border:0; border-collapse:separate; border-spacing:0; width:100%; background-color:#F0F1F2" width="100%" bgcolor="#F0F1F2"> <tr> <td style="font-family:sans-serif; font-size:14px; padding:0; vertical-align:top" valign="top">&#xA0;</td> <td class="container" style="font-family:sans-serif; font-size:14px; padding:0; display:block; max-width:500px; width:500px; margin:0 auto" valign="top" width="500"> <div class="content" style="box-sizing:border-box; display:block; margin:0 auto; max-width:500px; padding-bottom:10px"> <div class="logo centered" style="background-repeat:no-repeat; height:40px; margin-bottom:32px; margin-top:32px; width:110px; margin-left:auto; margin-right:auto; background-image:url()" height="40" width="110"></div> <table class="main" style="border:0; border-collapse:separate; border-spacing:0; width:100%; background:#fff; border-radius:5px" width="100%"> <tr> <td class="wrapper" style="font-family:sans-serif; font-size:14px; padding:32px; vertical-align:top; box-sizing:border-box" valign="top"> <table style="border:0; border-collapse:separate; border-spacing:0; width:100%" width="100%"> <tr> <td class="header" style="font-family:sans-serif; font-size:21px; padding:0; vertical-align:top; font-weight:normal; line-height:29px; padding-bottom:20px; text-align:center" valign="top" align="center"> Congrats! You have signed up an indeep account </td> </tr> <tr> <td style="font-family:sans-serif; font-size:14px; padding:0; vertical-align:top" valign="top"> <p style="color:#333; font-family:sans-serif; font-size:14px; font-weight:normal; margin:0; margin-bottom:15px; text-align:center" align="center">Please click this link to active your indeep account：</p> </td> </tr> <tr> <td style="font-family:sans-serif; font-size:14px; padding:0; vertical-align:top" valign="top"> <table class="btn" style="border:0; border-collapse:separate; border-spacing:0; width:100%; box-sizing:border-box" width="100%"> <tbody> <tr> <td align="left" style="font-family:sans-serif; font-size:14px; padding:0; vertical-align:top; padding-bottom:15px" valign="top"> <table class="btn-primary centered" style="border:0; border-collapse:separate; border-spacing:0; mso-table-lspace:0; mso-table-rspace:0; width:auto; margin-left:auto; margin-right:auto" width="auto"> <tbody> <tr> <td style="font-family:sans-serif; font-size:14px; padding:0; vertical-align:top; background-color:#fff; border-radius:5px; text-align:center" valign="top" bgcolor="#ffffff" align="center">
    <a href="http://`+myUrl+`/active?id=`;

    console.log(part1 );
      var part2 = `" target="_blank" style="color:#fff; text-decoration:none; background-color:#0199ff; border:solid 1px #008AE6; border-radius:3px; box-sizing:border-box; cursor:pointer; display:inline-block; font-size:19px; font-weight:normal; margin:0; padding:15px; border-color:#008AE6; line-height:24px; margin-bottom:20px; margin-top:20px; width:280px" bgcolor="#0199ff" width="280">Active your account</a>
    </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> <tr> <td style="font-family:sans-serif; font-size:14px; padding:0; vertical-align:top" valign="top"> </td> </tr> <tr class="ending" style="margin:0; margin-top:20px"> <td style="font-family:sans-serif; font-size:14px; padding:0; vertical-align:top" valign="top"> <p style="color:#333; font-family:sans-serif; font-size:14px; font-weight:normal; margin:0; margin-bottom:15px; text-align:justify" align="justify"> Have a good job！<br/>indeep team </p> </td> </tr> <tr> </tr> </table> </td> </tr> </table>
    `;


    console.log(part2 );
      var mail = {
        // sender
        from: "indeep<indeeptk@163.com>",
        // title , have to use Chinese because of the server settings, if no Chinese , mail will be banned.(considered junk mail)
        subject: "Active indeep account/Activer le compte indeep/激活indeep账号",
        // reciever
        to: req.body.user,
        // mail content, html /text
        text:
          "Please copy this url and paste it to your browser. URL: http:www."+myUrl+"/active/?id=" +myid.toString(),
        html: part1 + myid.toString() + part2
      };
      console.log(mail );
      // console.log(part1 + myid.toString() + part2);
      try {
        send(mail);
      } catch (error) {
        res.json({
          ret_code: 2,
          ret_msg: "sent mail failed!"
        });
      }
      res.json({
        ret_code: 0,
        ret_msg: "signup sucsess!"
      });
    };

  //router.post("/login", asyncHandler(async function(req, res, next) {

exports.postLogin =  async(req, res, next) => {
    // use asyncHandler to handle the errors, otherwise you have to write lots of try-catches
    // this post should be triggered by the login button(SUBMIT), so the response is json and
    //if login success,session will remember user logged in , browser use javascript to  refresh the page.(location.reload();) ,then / router render the login page.
    //if failed, session still not logged in, so browser may just alert something.
    //var sess = req.session;

    //console.log( User);
   var isUser = await User.findOne(
      {
        user: req.body.user.toLowerCase(),
        password: md5(req.body.password).toString(),
        status: "active"
      },
      function(err, docs) {
          console.log('docs~~~~~~~~~~~~:',docs )
      }
    );
    console.log(  req.body.user.toLowerCase(),md5(req.body.password).toString(), isUser);
    if (isUser) {
      req.session.regenerate(function(err) {
        if (err) {
          //console.log('login failed');
          return res.json({
            ret_code: 2,
            ret_msg: "Server error,login failed!"
          });
        }
        req.session.loginUser = isUser.user.toLowerCase();
        req.session.loginRole = isUser.type;
        res.json({
          ret_code: 0,
          ret_msg: "login sucsess!"
        });
      });
    } else {
      res.json({
        ret_code: 1,
        ret_msg: "Login information incorrect!"
      });
    }
  };
  

  exports.postUpload = (req,res , next ) => {
    if (!req.files)
    return res.status(400).send('No files were uploaded.');
    let file = req.files.file;
  

    var myid = new ObjectID();

    file.mv('./public/upload/'+myid+'.png' , function(err) {
      if (err)
      { 
        //return res.status(500).send(err);
        res.json({
          ret_code: 1,
          ret_msg: "move file failed!"
        });
      }
      
       console.log('go go go go~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~',myid,myuser.loginUser);

        User.findOneAndUpdate(
        {
          user:myuser.loginUser
        },
        {
          pix: myid
        },
        function(err, docs) {
          if (docs) {
            myuser.pix = myid;
            console.log('go go go go~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~',myuser.pix );

            res.json({
              ret_code: 0,
              ret_path: myid,
              ret_msg: "avatar Saved!"
            });
          } else {
            res.json({
              ret_code: 2,
              ret_msg: "Could not find user!"
            });
          }
        });

    
    });


  };


  exports.postChangePassword =  (req, res, next) => {
/*
    "user": hiddenLoginUser,
    "oldpasstext":oldpasstext,
    "passtext":passtext
*/
let  user=  req.body.user;
let oldpasstext = req.body.oldpasstext;
let passtext =  req.body.passtext;
console.log(user,oldpasstext,  passtext);

if (oldpasstext==  'd41d8cd98f00b204e9800998ecf8427e' ) //magic for md5('')
{
  oldpasstext = '';
}

User.findOneAndUpdate(
  {
    user:user,
    password: oldpasstext
  },
  {
    password:passtext
  },
  function(err, docs) {
    if (docs) {
      res.json({
        ret_code: 0,
        ret_msg: "password changed!"
      });
    } else {
      res.json({
        ret_code: 2,
        ret_msg: "password wrong!"
      });
    }
  });


  };
  