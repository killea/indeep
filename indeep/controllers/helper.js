"use strict";

module.exports =  function filterStr(str) {
  var pattern = new RegExp(
    "[`~!@$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？%_]"
  );
  var specialStr = "";
  for (var i = 0; i < str.length; i++) {
    specialStr += str.substr(i, 1).replace(pattern, "");
  }
  return specialStr.split(String.fromCharCode(92)).join(""); //for  \
};

