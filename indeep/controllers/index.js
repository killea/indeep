"use strict";

var express = require("express");
var session = require("express-session");
var mongoose = require("mongoose");
Promise = require("bluebird");
mongoose.Promise = Promise; //! use bluebird ,because mongoose.Promise is deprecated !
var router = express.Router();
var app = require("./app.js");
var MongoStore = require("connect-mongo")(session);
const asyncHandler = require("express-async-handler"); //handle the errors of async functions
//var md5 = require("crypto-js/md5");
//var ObjectID = require("mongodb").ObjectID;
//var send = require("./mail.js");
var GoogleStrategy = require("passport-google-oauth2").Strategy;
var fs = require('fs');
var ini = require('ini');
var config = ini.parse(fs.readFileSync('../indeep/config/config.ini', 'utf-8'));
var connectStr = config.setting.mongo;
var myUrl =config.setting.myurl;
const fileUpload = require('express-fileupload');

const databaseController = require('./database');
const apiController = require ( './api');

var User = require("../model/userModel.js");
//console.log(connectStr);
mongoose.connect(connectStr  ).then(() => {});

app.use(fileUpload());
app.use(
  session({
    secret: "hank_sign_za1d338*6~",
    resave: true,
    saveUninitialized: false,
    store: new MongoStore({
      mongooseConnection: mongoose.connection
    })
  })
);

/*
const myuser = {
  isLogin: false,
  loginUser: "guest",
  loginRole: "none", //root, user, boss, google, facebook ...
  pix:""
};*/
const myuser = require('./myuser');


//https://console.developers.google.com/apis/credentials?project=indeep-213905&folder&organizationId
var GOOGLE_CLIENT_ID =
  "1038484925731-iu338aiclri6kg7unj6tpn078gckbn61.apps.googleusercontent.com";
var GOOGLE_CLIENT_SECRET = "-IX-L-0ckMb7rcMCu54ZsUcB";
var passport = require("passport");
app.use(passport.initialize());
app.use(passport.session());

/*
passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.findById(id, function(err, user) {
    done(err, user);
  });
});*/

passport.serializeUser(function(user, done) {
  done(null, user);
});
passport.deserializeUser(function(obj, done) {
  done(null, obj);
});

passport.use(
  new GoogleStrategy(
    {
      clientID: GOOGLE_CLIENT_ID,
      clientSecret: GOOGLE_CLIENT_SECRET,
      //NOTE :
      //Carefull ! and avoid usage of Private IP, otherwise you will get the device_id device_name issue for Private IP during authentication
      //The workaround is to set up thru the google cloud console a fully qualified domain name such as http://mydomain:3000/
      //then edit your /etc/hosts local file to point on your private IP.
      //Also both sign-in button + callbackURL has to be share the same url, otherwise two cookies will be created and lead to lost your session
      //if you use it.
      callbackURL: "http://"+myUrl+"/auth/google/callback",
      passReqToCallback: true
    },
     function(req, accessToken, refreshToken, profile, done) {
      // asynchronous verification, for effect...
      process.nextTick( async function() {
        // To keep the example simple, the user's Google profile is returned to
        // represent the logged-in user.  In a typical application, you would want
        // to associate the Google account with a user record in your database,
        // and return that user instead.
        var sess = req.session;
        sess.loginUser = profile.email.toLowerCase();
        myuser.loginUser = sess.loginUser;
        myuser.isLogined = !!sess.loginUser;
        var tmpUser = await User.findOne(
          {
            user: myuser.loginUser.toLowerCase(),
            status: "active",
            google:myuser.loginUser.toLowerCase()
          },
          function(err, docs) {
          });

          if (tmpUser) {
            myuser.loginRole = tmpUser.type;
            if (!!tmpUser.pix  && tmpUser.pix != undefined)
            {
              myuser.pix = tmpUser.pix;
            }
            else{
              myuser.pix = 'avatar';
            }
            sess.loginRole = myuser.loginRole;
            console.log('sess.loginRole',sess.loginRole);
            return done(null, profile);
            
          } else {
            myuser.loginRole = '';
            sess.loginRole = '';
            return done(null, profile);
            };


        
      });
    }
  )
);

var checkLogin = async function(req, res, next) {
  var sess = req.session;
  myuser.loginUser = sess.loginUser;
  myuser.isLogined = !!sess.loginUser ;
  if (myuser.isLogined) {
       await User.findOne(
      {
        user: myuser.loginUser
      },
      function(err, docs) {

        if (!!docs){
          if (docs.pix!=undefined)
          { myuser.pix  =  docs.pix;
          }
          else
          {
          myuser.pix = "avatar";
          }
         
          myuser.loginRole = docs.type;
        }
        else
        {
          myuser.pix = "avatar";
        }
        console.log('CHECKLOGIN:',"user " + sess.loginUser + " logined! loginRole=", myuser.loginRole ,'sessionID:',req.session.id, 'myuser.pix:',myuser.pix); 
      });
  
  } else {
    console.log("user not logined! loginRole=",myuser.loginRole ,'sessionID:',req.session.id );
  }
  next();
};

app.use(checkLogin);

// GET /auth/google
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  The first step in Google authentication will involve
//   redirecting the user to google.com.  After authorization, Google
//   will redirect the user back to this application at /auth/google/callback
router.get("/auth/google",
  passport.authenticate("google", {
    scope: ["email"],
    prompt: "select_account"
  })
);
// GET /auth/google/callback
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  If authentication fails, the user will be redirected back to the
//   login page.  Otherwise, the primary route function function will be called,
//   which, in this example, will redirect the user to the home page.
router.get("/auth/google/callback",
  passport.authenticate("google", {
    successRedirect: "/setrole",
    failureRedirect: "/"
  })
);

/* GET */
router.get("/",apiController.getHome);
router.get("/jobdetail",apiController.getJobdetail);
router.get("/writeresume",   apiController.getWriteresume);
router.get("/postjob",apiController.getPostjob);
router.get("/cvrecived",apiController.getCvrecived);
router.get("/signupuser",apiController.getSignupuser);
router.get("/signuppostjob", apiController.getSignuppostjob);
router.get("/sentmail", apiController.getSentmail);
router.get("/active", apiController.getActive);
router.get("/job", apiController.getJob);
router.get("/logout", apiController.getLogout);
router.get("/setrole",  apiController.getSetrole);
router.get("/profile",  apiController.getProfile);

/* POST */
router.post("/database", databaseController.postDatabase);
router.post("/login", asyncHandler(databaseController.postLogin));
router.post( "/setrole", asyncHandler (databaseController.postSetRole));
router.post( "/signup", asyncHandler(databaseController.postSignup)) ;
router.post('/upload', databaseController.postUpload);
router.post('/changepassword', databaseController.postChangePassword);
module.exports =router;

