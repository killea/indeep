
'use strict';

var url = require("url");
const myuser = require('./myuser');
var mongoose = require('mongoose').set('debug', true);
var User = require("../model/userModel.js");

exports.getLogout =   (req, res, next) => {
    console.log( 'logout~~~~~~~~~~~~');
    req.session.destroy(function(err) {
      if (err) {
        res.json({
          ret_code: 2,
          ret_msg: "logout failed"
        });
        return;
      }
      res.redirect("/");
    });
  };
  exports.getJob = (req, res, next) =>{
    var parseObj = url.parse(req.url, true);
    let what = parseObj.query.what;
    let where = parseObj.query.where;
    //#endregion
     if (myuser.isLogined) {
      res.render("job", {
        isLogined: true,
        loginUser: myuser.loginUser,  
        loginRole: myuser.loginRole,
        pix:myuser.pix,
        whatText: what,
        whereText: where
      });
    } else {
      res.render("job", {
        isLogined: false,
        whatText: what,
        whereText: where
      });
    }
  };


  exports.getActive= (req, res, next) =>{
    let url = req.url;
    let myId = url.replace("/active?id=", "");
  
    //console.log(myId);
    var objId = mongoose.Types.ObjectId(myId);
  
    console.log(objId);
  
    User.findOneAndUpdate(
      {
        _id: objId,
        status: "null"
      },
      {
        status: "active"
      },
      function(err, finduser) {
        if (!!finduser && myuser.isLogined == false) {
          res.render("active", {
            isLogined: false,
            myusername: finduser.user
          });
        } else {
          res.render("notice", {
            isLogined: myuser.isLogined,
            pix:myuser.pix,
            notice: "Active account failed!"
          });
        }
      }
    );
  };



  
exports.getSentmail=(req, res, next) =>{
  if (myuser.isLogined) {
    res.render("sentmail", {
      isLogined: true,
      loginUser: myuser.loginUser,
      pix:myuser.pix
    });
  } else {
    res.render("sentmail", {
      isLogined: false
    });
  }
};


/*
exports.getAboutus =(req, res, next) =>{
  if (myuser.isLogined) {
    res.render("aboutus", {
      isLogined: true,
      loginUser: myuser.loginUser,
      pix:myuser.pix
    });
  } else {
    res.render("aboutus", {
      isLogined: false
    });
  }
};
*/


exports.getSignuppostjob = (req, res, next) =>{
  var str = "Sign up as a employer who wants to post jobs";

  if (myuser.isLogined) {
    //cannot signup again
    res.render("index", {
      isLogined: true,
      loginUser: myuser.loginUser,
      loginRole: myuser.loginRole,
      pix:myuser.pix
    });
    return;
  } else {
    res.render("signup", {
      isLogined: false,
      typestr: str,
      type: "boss"
    });
  }
};


exports.getSignupuser = (req, res, next) =>{
  var str = "Sign up as a user who is looking for jobs";
  if (myuser.isLogined) {
    //cannot signup again
    res.render("index", {
      isLogined: true,
      loginUser: myuser.loginUser,
      loginRole: myuser.loginRole,
      pix:myuser.pix
    });
    return;
  } else {
    res.render("signup", {
      isLogined: false,
      typestr: str,
      type: "user"
    });
  }
};


exports.getCvrecived = (req, res, next)=> {
  res.render("cvrecived", {
    isLogined: myuser.isLogined,
    loginUser: myuser.loginUser,
    loginRole: myuser.loginRole,
    pix:myuser.pix
  });
};

 exports.getPostjob= (req, res, next)=> {
  res.render("postjob", {
    isLogined: myuser.isLogined,
    loginUser: myuser.loginUser,
    loginRole: myuser.loginRole,
    pix:myuser.pix
  });
};


exports.getWriteresume = (req, res, next) =>{
  if (myuser.isLogined) {
    res.render("writeresume", {
      isLogined: true,
      loginUser: myuser.loginUser,
      loginRole: myuser.loginRole,
      pix:  myuser.pix
    });
  } else {
    res.render("index", {
      isLogined: false,
      loginUser: myuser.loginUser,
      loginRole: myuser.loginRole
    });
  }
};



exports.getJobdetail = (req, res, next)=> {
  if (myuser.isLogined) {
    res.render("jobdetail", {
      isLogined: true,
      loginUser: myuser.loginUser,
      loginRole: myuser.loginRole,
      pix:myuser.pix
    });
  } else {
    res.render("jobdetail", {
      isLogined: false
    });
  }
};

exports.getHome = (req, res, next) => {
  //console.log(data);3
  console.log(
    "homelogin_log:",
    myuser.loginUser,
    myuser.loginRole,
    myuser.isLogined,
    myuser.pix
  );

  console.log('getHome!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!',myuser);

  if (myuser.isLogined) {
    res.render("index", {
      isLogined: true,
      loginUser: myuser.loginUser,
      loginRole: myuser.loginRole,
      pix: myuser.pix
    });
  } else {
    //console.log('myuser.isLogined : false');
    res.render("index", {
      isLogined: false,
      loginUser: myuser.loginUser
    });
  }
};






exports.getSetrole = (req, res, next) =>{ 
  console.log("come to ~~~~~~~~~~~~~  setrole:",myuser.loginUser,myuser.loginRole,myuser.isLogined, myuser.pix);
  if ( myuser.loginUser ==undefined)
  {
    res.redirect("/");
  }
  
  var isUser =  User.findOne(
    {
      user: myuser.loginUser.toLowerCase(),
      status: "active",
      google:myuser.loginUser.toLowerCase()
    },
    function(err, docs) {

      if (!!docs) {
        myuser.loginRole = isUser.type;
        req.session.loginRole = isUser.type;
        res.redirect("/");
      } else {
        res.render("setrole", {
          isLogined: true,
          loginUser: myuser.loginUser,
          loginRole: ''
    
        });
      }


    }
  );
};


exports.getProfile = (req, res, next) => {
  //let pix= '/upload/avatar.png';
  console.log('getProfile===',myuser); 
  res.render("profile",{
    isLogined: myuser.isLogined,
    loginUser: myuser.loginUser,
    loginRole: myuser.loginRole,
    pix: myuser.pix
  });
};