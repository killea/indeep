//const assert = require('assert');
const request = require('supertest');
const server = require('../bin/test');

describe('GET /', () => {
  it('should return 200 OK', (done) => {
    request(server)
      .get('/')
      .expect(200, done);
  });
});

describe('GET /jobdetail', () => {
  it('should return 200 OK', (done) => {
    request(server)
      .get('/jobdetail')
      .expect(200, done);
  });
});

describe('GET /writeresume', () => {
  it('should return 200 OK', (done) => {
    request(server)
      .get('/writeresume')
      .expect(200, done);
  });
});

describe('GET /postjob', () => {
  it('should return 200 OK', (done) => {
    request(server)
      .get('/postjob')
      .expect(200, done);
  });
});


describe('GET /cvrecived', () => {
  it('should return 200 OK', (done) => {
    request(server)
      .get('/cvrecived')
      .expect(200, done);
  });
});

describe('GET /signupuser', () => {
  it('should return 200 OK', (done) => {
    request(server)
      .get('/signupuser')
      .expect(200, done);
  });
});

describe('GET /signuppostjob', () => {
  it('should return 200 OK', (done) => {
    request(server)
      .get('/signuppostjob')
      .expect(200, done);
  });
});


describe('get /signupuser', () => {
  it('should return 200 OK', (done) => {
    request(server)
      .get('/signupuser')
      .expect(200, done);
  });
});


describe('get /sentmail', () => {
  it('should return 200 OK', (done) => {
    request(server)
      .get('/sentmail')
      .expect(200, done);
  });
});

/*
describe('get /active', () => {
  it('should return 200 OK', (done) => {
    request(server)
      .get('/active')
      .expect(200, done);
  });
});
*/



describe('get /job', () => {
  it('should return 200 OK', (done) => {
    request(server)
      .get('/job')
      .expect(200, done);
  });
});

/*
describe('POST /database', () => {
  it('should return 200 OK', (done) => {
    request(server)
      .post('/database')
      .expect(200, done);
  });
});
*/

describe('GET /fake-url', () => {
  it('should return 404', (done) => {
    request(server)
      .get('/fake-url')
      .expect(404, done);
  });
});

describe('POST /login', () => {
  it("should return {ret_code: 0, ret_msg: 'login sucsess!'} OK", (done) => {
    request(server)
      .post('/login')
      .send({ 'user': 'test', 'password':'test'})
      .expect({
        ret_code: 0,
        ret_msg: 'login sucsess!'
      }, done);
  });
});


describe('POST /login', () => {
  it("should return {ret_code: 1, ret_msg: 'Login information incorrect!'} OK", (done) => {
    request(server)
      .post('/login')
      .send({ 'user': 'test', 'password':'test2'})
      .expect({
        ret_code: 1,
        ret_msg: 'Login information incorrect!'
      }, done);
  });
});



